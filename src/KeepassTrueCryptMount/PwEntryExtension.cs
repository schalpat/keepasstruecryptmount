﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassTrueCryptMount
{
    using System;
    using System.Text;

    using KeePassLib;
    using KeePass.Util;

    internal static class PwEntryExtension
    {
        public static string ToTrueCryptArguments(this PwEntry entry)
        {
            var arguments = new StringBuilder();

            // beep
            {
                var beepString = entry.Strings.GetSafe(EntryStrings.Beep).ReadString();
                bool beep;
                bool.TryParse(beepString, out beep);
                if (beep)
                {
                    arguments.Append(" /beep ");
                }
            }

            // readonly
            {
                var readonlyString = entry.Strings.GetSafe(EntryStrings.Readonly).ReadString();
                bool readOnly;
                bool.TryParse(readonlyString, out readOnly);
                if (readOnly)
                {
                    arguments.Append(" /m ro ");
                }
            }

            // removable
            {
                var removableString = entry.Strings.GetSafe(EntryStrings.Removable).ReadString();
                bool removable;
                bool.TryParse(removableString, out removable);
                if (removable)
                {
                    arguments.Append(" /m rm ");
                }
            }

            // explore
            {
                var exploreString = entry.Strings.GetSafe(EntryStrings.Explorer).ReadString();
                bool explore;
                bool.TryParse(exploreString, out explore);
                if (explore)
                {
                    arguments.Append(" /explore ");
                }
            }

            // silent
            {
                var silentString = entry.Strings.GetSafe(EntryStrings.Silent).ReadString();
                bool silent;
                bool.TryParse(silentString, out silent);
                if (silent)
                {
                    arguments.Append(" /silent ");
                }
            }

            // quit
            {
                var quitString = entry.Strings.GetSafe(EntryStrings.Background).ReadString();
                bool quit;
                bool.TryParse(quitString, out quit);
                if (quit)
                {
                    arguments.Append(" /q backgound ");
                }
            }

            // drive letter
            {
                var driveString = entry.Strings.GetSafe(EntryStrings.DriveLetter).ReadString();
                if (string.IsNullOrEmpty(driveString))
                {
                    arguments.Append(" /auto ");
                }
                else
                {
                    arguments.Append(" /letter ").Append(driveString[0]).Append(" ");
                }
            }

            // container volume
            {
                var volumeString = entry.Strings.GetSafe(EntryStrings.Volume).ReadString();
                arguments.Append(" /v ");
                if (volumeString.Contains(" ") && !volumeString.StartsWith("\"") && !volumeString.EndsWith("\""))
                {
                    arguments.AppendFormat("\"{0}\"", volumeString);
                }
                else
                {
                    arguments.Append(volumeString);
                }
            }

            // Keyfiles
            {
                var keyFilesString = entry.Strings.GetSafe(EntryStrings.KeyFiles).ReadString();
                String[] keyFiles = keyFilesString.Split(';');
                for (int i = 0; i < (keyFiles.Length - 1); i++ )
                {
                    arguments.Append(" /k ");
                    if (keyFiles[i].Contains(" ") && !keyFiles[i].StartsWith("\"") && !keyFiles[i].EndsWith("\""))
                    {
                        arguments.AppendFormat("\"{0}\"", keyFiles[i]);
                    }
                    else
                    {
                        arguments.Append(keyFiles[i]);
                    }

                }
               
            }
            return arguments.ToString();
        }

        public static bool ShouldHideMountDialog(this PwEntry entry)
        {
            if (entry == null)
            {
                return true;
            }

            bool hideDialog;
            var hideDialogString = entry.Strings.GetSafe(EntryStrings.MountWithoutDialog);

            if (!hideDialogString.IsEmpty)
            {
                bool.TryParse(hideDialogString.ReadString(), out hideDialog);
            }
            else
            {
                hideDialog = false;
            }

            return hideDialog;
        }

        public static bool HasMountSettings(this PwEntry entry)
        {
            if (entry == null)
            {
                return false;
            }

            return bool.TrueString.Equals(entry.Strings.GetSafe(EntryStrings.Enabled).ReadString());
        }

        public static void PerformPasswordAutotype(this PwEntry entry,PwDatabase database)
        {
            var askForPasswordString = entry.Strings.GetSafe(EntryStrings.AskForPassword).ReadString();
            bool askForPassword;
            bool.TryParse(askForPasswordString, out askForPassword);

            if (!askForPassword)
            {
                AutoType.PerformIntoCurrentWindow(entry,database);
            }
        }
    }
}
