﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassTrueCryptMount
{
    /* 
     * It seems like a mistake, but the plugin namespace must the same
     * than the plugin class name (with suffix Ext), so no dots allowed. 
     * The assembly file must also have the name of the namespace.
     */

    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    using KeePass.Plugins;

    public sealed class KeePassTrueCryptMountExt : Plugin
    {
        private IPluginHost pluginHost;
        private ToolStripMenuItem mnuEntryTrueCryptMount;
        private ToolStripMenuItem mnuToolsTrueCryptOptions;
        private MountMenuItemState mountMenuItemState;
        
        public KeePassTrueCryptMountExt()
        {
        }

        public override Image SmallIcon
        {
            get
            {
                return TrueCryptInfo.ExecutableExists(this.pluginHost.GetTrueCryptExecutable())
                    ? Resources.TrueCryptNormal
                    : Resources.TrueCryptError;
            }
        }

        public override string UpdateUrl
        {
            get
            {
                return @"https://bytebucket.org/schalpat/keepasstruecryptmount/wiki/version_manifest.txt";
            }
        }

        public override bool Initialize(IPluginHost host)
        {
            this.pluginHost = host;

            // insert the options menu item inside of the tools menu.
            var toolsMenuItem = this.pluginHost.MainWindow.ToolsMenu;

            this.mnuToolsTrueCryptOptions = new ToolStripMenuItem(LanguageTexts.TCOptionsMenuItemText + LanguageTexts.MenuItemOpenDialogSuffix);
            this.mnuToolsTrueCryptOptions.Image = Resources.TrueCryptNormal;
            this.mnuToolsTrueCryptOptions.Click += this.OnTrueCryptOptionsMenuItemClicked;

            toolsMenuItem.DropDownItems.Add(this.mnuToolsTrueCryptOptions);

            // insert the mount menu item inside of the entry menu.
            this.pluginHost.MainWindow.EntryContextMenu.KeyDown += this.OnEntryContextMenuKeyDown;
            this.pluginHost.MainWindow.EntryContextMenu.KeyUp += this.OnEntryContextMenuKeyUp;
            this.pluginHost.MainWindow.EntryContextMenu.Opened += this.OnEntryContextMenuOpened;
            this.mnuEntryTrueCryptMount = new ToolStripMenuItem();
            this.mnuEntryTrueCryptMount.ShortcutKeys = Keys.Control | Keys.T;
            this.mnuEntryTrueCryptMount.ShowShortcutKeys = true;
            this.mnuEntryTrueCryptMount.Click += this.OnTrueCryptMenuItemClicked;
            this.mnuEntryTrueCryptMount.Image = Resources.TrueCryptNormal;
            this.pluginHost.MainWindow.EntryContextMenu.Items.Insert(4, this.mnuEntryTrueCryptMount);
            this.mountMenuItemState = new MountMenuItemState(this.mnuEntryTrueCryptMount);
            this.mountMenuItemState.ChangeState(MountMenuItemStates.Invisible);
            this.mountMenuItemState.AlwaysVisible = this.pluginHost.GetTrueCryptMenuItemAlwaysVisible();

            this.SetTrueCryptMountMenuMenuAppearance();

            return true;
        }
        
        public override void Terminate()
        {
            this.pluginHost.MainWindow.ToolsMenu.DropDownItems.Remove(this.mnuToolsTrueCryptOptions);
            this.mnuToolsTrueCryptOptions.Click -= this.OnTrueCryptOptionsMenuItemClicked;
            this.mnuToolsTrueCryptOptions.Dispose();
            
            this.pluginHost.MainWindow.EntryContextMenu.KeyDown -= this.OnEntryContextMenuKeyDown;
            this.pluginHost.MainWindow.EntryContextMenu.KeyUp -= this.OnEntryContextMenuKeyUp;
            this.pluginHost.MainWindow.EntryContextMenu.Opened -= this.OnEntryContextMenuOpened;
            this.pluginHost.MainWindow.EntryContextMenu.Items.Remove(this.mnuEntryTrueCryptMount);
            this.mnuEntryTrueCryptMount.Click -= this.OnTrueCryptMenuItemClicked;
            this.mnuEntryTrueCryptMount.Dispose();
            base.Terminate();
        }

        private void OnEntryContextMenuKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
            {
                this.SetTrueCryptMountMenuItemAccessibility(true);
            }
        }
        
        private void OnEntryContextMenuKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
            {
                this.SetTrueCryptMountMenuItemAccessibility();
            }
        }

        private void OnEntryContextMenuOpened(object sender, EventArgs e)
        {
            this.SetTrueCryptMountMenuItemAccessibility();
        }

        private void OnTrueCryptMenuItemClicked(object sender, EventArgs e)
        {
            var entry = this.pluginHost.MainWindow.GetSelectedEntry(false);
            var database = this.pluginHost.Database;

            if (entry == null)
            {
                return;
            }

            if (this.mountMenuItemState.ShouldMountDialogShown)
            {
                var dialog = new TrueCryptMountForm()
                    .WithShowInTaskBar(false)
                    .WithStartPosition(FormStartPosition.CenterParent)
                    .ReadFrom(entry);

                var result = dialog.ShowDialog(this.pluginHost.MainWindow);

                if (result == DialogResult.Cancel)
                {
                    // exit here, save nothing, mount nothing
                    return;
                }

                // save settings
                if (dialog.ChangesApplied)
                {
                    dialog.WriteTo(entry);
                    entry.Touch(true);
                    this.pluginHost.Database.Modified = true;
                    this.pluginHost.MainWindow.UpdateUI(false, null, false, null, false, null, true);
                }
            }

            // do mount...
            var truecryptProcessInfo = new ProcessStartInfo(this.pluginHost.GetTrueCryptExecutable(),entry.ToTrueCryptArguments());

            var truecryptProcess = Process.Start(truecryptProcessInfo);
            if(truecryptProcess != null)
            {
                try
                {
                    if (truecryptProcess.WaitForInputIdle(this.pluginHost.GetTrueCryptAutoTypeWaitTimeout()))
                    {
                        entry.PerformPasswordAutotype(database);
                    }
                }
                catch (InvalidOperationException ex)
                {

                }
            }

        }

        private void OnTrueCryptOptionsMenuItemClicked(object sender, EventArgs e)
        {
            // open the plugin options dialog.
            using (var optionsDialog = new TrueCryptOptionsForm(this.pluginHost))
            {
                optionsDialog.WithShowInTaskBar(false)
                             .WithStartPosition(FormStartPosition.CenterParent)
                             .ShowDialog(this.pluginHost.MainWindow);
            }

            this.mountMenuItemState.AlwaysVisible = this.pluginHost.GetTrueCryptMenuItemAlwaysVisible();

            this.SetTrueCryptMountMenuMenuAppearance();
        }

        /// <summary>
        /// Changes the appearance and accessibilty of the mount menu item after option change.
        /// </summary>
        private void SetTrueCryptMountMenuMenuAppearance()
        {
            // in case of missing executeable, disable the menu item and load another overlay image.
            if (!TrueCryptInfo.ExecutableExists(this.pluginHost.GetTrueCryptExecutable()))
            {
                this.mnuEntryTrueCryptMount.Enabled = false;
                this.mnuEntryTrueCryptMount.Image = Resources.TrueCryptError;
            }
            else
            {
                this.mnuEntryTrueCryptMount.Enabled = true;
                this.mnuEntryTrueCryptMount.Image = Resources.TrueCryptNormal;
            }
        }

        private void SetTrueCryptMountMenuItemAccessibility(bool shiftHold = false)
        {
            var state = this.mountMenuItemState.GetState(
                this.pluginHost.MainWindow.GetSelectedEntriesCount(),
                this.pluginHost.MainWindow.GetSelectedEntry(false),
                shiftHold);

            this.mountMenuItemState.ChangeState(state);
        }
    }
}
